# Find biggest hard drive size
echo "Biggest disk found:"
lsblk | grep disk | grep G | tr -s ' ' | cut -d' ' -f 4 | sort -n | tail -n 1
# Find name of biggest hard drive name and assign it to variable
# lsblk | grep part | grep "$(lsblk | grep disk | grep G | tr -s ' ' | cut -d' ' -f 4 | sort -n | tail -n 1)" | tr -s ' ' | cut -d' ' -f 1 | tr -dc '[:alnum:]\n\r'
DEVICE=$(lsblk | grep part | grep "$(lsblk | grep disk | grep G | tr -s ' ' | cut -d' ' -f 4 | sort -n | tail -n 1)" | tr -s ' ' | cut -d' ' -f 1 | tr -dc '[:alnum:]\n\r')
echo "Corresponding partition name:"
echo $DEVICE

echo "If all data is correct any key to continue, otherwise Ctrl-C to exit"
while [ true ] ; do
read -t 3 -n 1
if [ $? = 0 ] ; then
exit ;
else
echo "waiting for the keypress"
fi
done

./test.sh $DEVICE

#./copy-rootfs-ssd_device.sh $DEVICE
cp ./data_device/setssdroot_device.service ./data_device/setssdroot.service
sed -i "s/DEVICE/$DEVICE/g" data_device/setssdroot.service

#cat data_device/setssdroot_device.service
#./setup-service_device.sh $DEVICE
